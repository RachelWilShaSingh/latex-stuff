# LaTeX stuff

sty files and templates that I use for class creation and stuff.

**rachdiagrams.sty** 
* Pre-defined diagrams for venn diagrams
* A combinatorics classification table (permutation / combination-set / ordered list / unordered list) and a blank table for fill-in
* Combinatorics equation summary table
* A table of all cards in a deck
* Tikzpicture definitions of red and green dice


**rachwidgets.sty**

* Pre-defined colors...
    * Colors for my document stylings
    * Colors for color-blindness-friendliness
    * Cell colors for truth tables (true/false values) using color-blind-friendly colors
* Text formatting...
    * Inline hint style
    * Easy tab object
    * Easy "fill in the blank" boject
    * Introductory box style
    * Hint box style
    * Error box style
    * Quick citation formatter
* Exam/quiz formatting...
    * Answer key styling, toggles for answer key
    * Styling to hide items when answer key is active (i.e., don't show entire assignment text)
    * Styling answers in red
* Grading formatting...
    * Grader boxes - checkboxes of 0, 1, 2, 3, and 4, corresponding to weight received on a question. (0, 25%, 50%, 75%, 100% credit)
    * Exam question format
    * Non-graded question format
    * Grading table row
* Code formatting...
    * C++ code frame
    * Python code frame
    * Example program output frmae
    * Example text output file frame
* Layout helpers...
    * Two-column view